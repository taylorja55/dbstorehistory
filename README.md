# Purpose #

This workspace is built to display and exercise the capability of storing and retreiving user interactions throughout the system. This will be stored without any code changes needed by the developers and will show all the inserts, updates and deletes to the database. This will NOT store any interactions that do not actually deal with the data for your system, however it will allow a DBA to retreive user history against the data of your software.

---
## How it works
The steps to make this properly work is as follows:

    Create a history table of the table you want to store all the history for by mirroring the table into a hist_<TableName> of all the attributes from the parent <Table>
	Remove all the key data from the <Parent Table> for your <History Table>
	Add 4 attributes into the <History Table> 
	    hist_id (Main index for this history table
		action_date (date & time of transaction)
		action_user (user that performed the transaction on the parent table)
		action_taken (Action performed; INSERT, UPDATE, DELETE)
	Create 2 functions (that accept an argument) for performing the inserts into the <history table> 
	    one with NEW. Data and one with OLD. Data
	Create 3 triggers that pass the action_taken into the above functions 
	    NOTE for UPDATE and INSERT call the .NEW function with the approperate action_taken. For the DELETE call the .OLD function with the action_taken = DELETE
	Test the operation by creting dummy inserts, updates and deletes into the parent table.

---
## postgreSQL Code to create

    CREATE or REPLACE FUNCTION <schema>.tg_fn_<parentTableName>()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

    INSERT INTO <schema>.hist_<parentTableName> (attributes of parent table) 
        action_date, action_user, action_taken) 
    VALUES (NEW.(attributes of parent table)
        NOW(), user, arg_value);
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';

    CREATE or REPLACE FUNCTION <schema>.tg_fn_del_<parentTableName>()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

    INSERT INTO <schema>.hist_<parentTableName> (attributes of parent table) 
        action_date, action_user, action_taken) 
    VALUES (OLD.(attributes of parent table)
        NOW(), user, arg_value);
    RETURN OLD;
    END;
    $$
    LANGUAGE 'plpgsql';

    CREATE TRIGGER tg_insert_<parentTableName>
    BEFORE INSERT
    ON <schema>.<parentTable>
    FOR EACH ROW
    EXECUTE PROCEDURE <schema>.tg_fn_<parentTable>('INSERT');

---
## Table Relationship Diagram 
Database [Image of Model](https://github.com/taylja/images/blob/main/namingConventionWithHistory.png)

