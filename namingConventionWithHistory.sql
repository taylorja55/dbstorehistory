/* ---------------------------------------------------- 	*/
/*  DBMS       : PostgreSQL						*/
/* ---------------------------------------------------- 	*/

/* Drop Views */

-- DROP VIEW IF EXISTS naming_convention."vwCustomerAddresses" CASCADE;

-- DROP VIEW IF EXISTS naming_convention."vwCustomerOrders_AllDetails" CASCADE;

-- DROP VIEW IF EXISTS naming_convention."vwCustomerOrderStatuses" CASCADE;

/* Drop Sequences for Autonumber Columns */

 

/* Drop Tables */

-- DROP TABLE IF EXISTS naming_convention.customerorderstatus CASCADE;

-- DROP TABLE IF EXISTS naming_convention.customer CASCADE;

-- DROP TABLE IF EXISTS naming_convention.customeraddress CASCADE;

-- DROP TABLE IF EXISTS naming_convention.customerorder CASCADE;

-- DROP TABLE IF EXISTS naming_convention.list_states CASCADE;

-- DROP TABLE IF EXISTS naming_convention.product CASCADE;

-- DROP TABLE IF EXISTS naming_convention.supplier CASCADE;

-- DROP TABLE IF EXISTS naming_convention.xref_customerorder_product CASCADE;

-- DROP TABLE IF EXISTS naming_convention.hist_customerorderstatus CASCADE;

-- DROP TABLE IF EXISTS naming_convention.hist_customer CASCADE;

-- DROP TABLE IF EXISTS naming_convention.hist_customeraddress CASCADE;

-- DROP TABLE IF EXISTS naming_convention.hist_customerorder CASCADE;

-- DROP TABLE IF EXISTS naming_convention.hist_product CASCADE;

-- DROP TABLE IF EXISTS naming_convention.hist_supplier CASCADE;

/* *** Create Tables *** */

CREATE TABLE naming_convention.customerorderstatus
(
	id bigserial NOT NULL, -- The primary key field for table.  The datatype of BIGSERIAL is a special type in that when the table is generated PostgreSQL will automatically create a sequence object that handles the automatically numbering for the column.
	customerorder_id bigint NULL,
	status_date date NULL,
	status_code varchar(10) NULL,
	comments text NULL
);

CREATE TABLE naming_convention.customer
(
	id bigserial NOT NULL,	-- The primary key field for table.  The datatype of BIGSERIAL is a special type in that when the table is generated PostgreSQL will automatically create a sequence object that handles the automatically numbering for the column.
	first_name varchar(50) NULL,
	middle_name varchar(50) NULL,
	surname varchar(50) NULL,
	date_of_birth date NULL,
	phone_no varchar(20) NULL,
	mobile_no varchar(20) NULL,
	email varchar(100) NULL,
	comments text NULL
);

CREATE TABLE naming_convention.customeraddress
(
	id bigserial NOT NULL, -- The primary key field for table.  The datatype of BIGSERIAL is a special type in that when the table is generated PostgreSQL will automatically create a sequence object that handles the automatically numbering for the column.
	customer_id serial NOT NULL,
	billing_or_delivery bit(1) NULL   DEFAULT '1',
	active bit(1) NULL,
	line_1 varchar(50) NULL,
	line_2 varchar(50) NULL,
	line_3 varchar(50) NULL,
	city varchar(100) NULL   DEFAULT 'Minot',
	state_id bigint NULL   DEFAULT 34,
	zip_postcode varchar(10) NULL,
	country varchar(100) NULL   DEFAULT 'USA',
	comments text NULL
);

CREATE TABLE naming_convention.customerorder
(
	id bigserial NOT NULL, -- The primary key field for table.  The datatype of BIGSERIAL is a special type in that when the table is generated PostgreSQL will automatically create a sequence object that handles the automatically numbering for the column.
	customer_id serial NOT NULL,
	order_date date NULL,
	paid_date date NULL,
	shipped_date date NULL,
	total_price double precision NULL,
	special_instructions varchar(255) NULL,
	consignment_no varchar(50) NULL,
	comments text NULL
);

CREATE TABLE naming_convention.list_states
(
	id bigint NOT NULL,
	name varchar(50) NULL
);

CREATE TABLE naming_convention.product
(
	id bigserial NOT NULL, -- The primary key field for table.  The datatype of BIGSERIAL is a special type in that when the table is generated PostgreSQL will automatically create a sequence object that handles the automatically numbering for the column.
	short_description varchar(255) NULL,
	unit_price double precision NULL,
	long_description text NULL,
	supplier_code varchar(20) NULL,
	supplier_partno varchar(50) NULL,
	comments text NULL
);

CREATE TABLE naming_convention.supplier
(
	supplier_code varchar(20) NOT NULL,
	supplier_name varchar(50) NULL,
	contact_name varchar(100) NULL,
	contact_phone_no varchar(20) NULL,
	contact_email varchar(100) NULL,
	address_line1 varchar(50) NULL,
	address_line2 varchar(50) NULL,
	address_line3 varchar(50) NULL,
	city varchar(100) NULL   DEFAULT 'Minot',
	state_id bigint NULL   DEFAULT 34,
	zip_postcode varchar(10) NULL,
	country varchar(50) NULL   DEFAULT 'USA',
	comments text NULL
);

CREATE TABLE naming_convention.xref_customerorder_product
(
	customerorder_id bigint NOT NULL,
	product_id bigint NOT NULL,
	quantity double precision NULL,
	unit_price double precision NULL,
	tax_amount double precision NULL,
	discount_percentage double precision NULL,
	comments text NULL
);

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE naming_convention.customerorderstatus ADD CONSTRAINT pk_customerorderstatus
	PRIMARY KEY (id);

CREATE INDEX ix_customerorderstatus_status ON naming_convention.customerorderstatus (status_code ASC);

CREATE INDEX ix_customerorderstatus_date ON naming_convention.customerorderstatus (status_date ASC);

CREATE TRIGGER tg_customerorderstatus_shipped
  AFTER INSERT ON naming_convention.customerorderstatus
  FOR EACH ROW EXECUTE PROCEDURE naming_convention.send_customer_notification(status_code);

ALTER TABLE naming_convention.customer ADD CONSTRAINT pk_customer
	PRIMARY KEY (id);

ALTER TABLE naming_convention.customer 
  ADD CONSTRAINT uq_customer_email UNIQUE (email);

CREATE INDEX ix_customer_surname ON naming_convention.customer (surname ASC);

CREATE INDEX ix_customer_mobile ON naming_convention.customer (mobile_no ASC);

ALTER TABLE naming_convention.customeraddress ADD CONSTRAINT pk_customeraddress
	PRIMARY KEY (id);

CREATE INDEX "IXFK_customeraddress_list_states" ON naming_convention.customeraddress (state_id ASC);

CREATE INDEX ixfk_customeraddress_customer ON naming_convention.customeraddress (customer_id ASC);

ALTER TABLE naming_convention.customerorder ADD CONSTRAINT pk_customerorder
	PRIMARY KEY (id);

ALTER TABLE naming_convention.customerorder 
  ADD CONSTRAINT uq_customerorder_consignment UNIQUE (consignment_no);

ALTER TABLE naming_convention.list_states ADD CONSTRAINT pk_states
	PRIMARY KEY (id);

ALTER TABLE naming_convention.product ADD CONSTRAINT pk_product
	PRIMARY KEY (id);

ALTER TABLE naming_convention.product 
  ADD CONSTRAINT uq_product_partno UNIQUE (supplier_partno);

ALTER TABLE naming_convention.supplier ADD CONSTRAINT pk_supplier
	PRIMARY KEY (supplier_code);

CREATE INDEX ix_supplier_sname ON naming_convention.supplier (supplier_name ASC);

CREATE INDEX ix_supplier_email ON naming_convention.supplier (contact_email ASC);

CREATE INDEX ix_supplier_cname ON naming_convention.supplier (contact_name ASC);

ALTER TABLE naming_convention.xref_customerorder_product ADD CONSTRAINT pk_customerorder_product
	PRIMARY KEY (customerorder_id,product_id);

ALTER TABLE naming_convention.xref_customerorder_product ADD CONSTRAINT ck_customerorder_product_discount CHECK ((discount_percentage >= (0)::double precision));

/* Create Foreign Key Constraints */

ALTER TABLE naming_convention.customerorderstatus ADD CONSTRAINT fk_customerorderstatus_customerorder
	FOREIGN KEY (customerorder_id) REFERENCES naming_convention.customerorder (id) ON DELETE No Action ON UPDATE No Action;

ALTER TABLE naming_convention.customeraddress ADD CONSTRAINT fk_customeraddress_list_states
	FOREIGN KEY (state_id) REFERENCES naming_convention.list_states (id) ON DELETE No Action ON UPDATE No Action;

ALTER TABLE naming_convention.customeraddress ADD CONSTRAINT fk_customeraddress_customer
	FOREIGN KEY (customer_id) REFERENCES naming_convention.customer (id) ON DELETE No Action ON UPDATE No Action;

ALTER TABLE naming_convention.customerorder ADD CONSTRAINT fk_customerorder_customer
	FOREIGN KEY (customer_id) REFERENCES naming_convention.customer (id) ON DELETE No Action ON UPDATE No Action;

ALTER TABLE naming_convention.product ADD CONSTRAINT fk_productsupplier
	FOREIGN KEY (supplier_code) REFERENCES naming_convention.supplier (supplier_code) ON DELETE No Action ON UPDATE No Action;

ALTER TABLE naming_convention.xref_customerorder_product ADD CONSTRAINT fk_customerorder_product_customerorder
	FOREIGN KEY (customerorder_id) REFERENCES naming_convention.customerorder (id) ON DELETE No Action ON UPDATE No Action;

ALTER TABLE naming_convention.xref_customerorder_product ADD CONSTRAINT fk_customerorder_product_product
	FOREIGN KEY (product_id) REFERENCES naming_convention.product (id) ON DELETE No Action ON UPDATE No Action;

/* Create Table Comments, Sequences for Autonumber Columns */

COMMENT ON TABLE naming_convention.customer
	'This table records information about customers. ';

COMMENT ON COLUMN naming_convention.customer.id
	'The primary key field for the table.  The datatype of BIGSERIAL is a special type in that when the table is generated PostgreSQL will automatically create a sequence object that handles the automatically numbering for the column.';

COMMENT ON TABLE naming_convention.customeraddress
	'This table records information about customer addresses and has been designed to allow for the ability to define multiple addresses per customers, along with the ability to specify if each address is a billing or delivery one.';

COMMENT ON TABLE naming_convention.list_states
	'This is a list table of all the selectable states';

/* *** Create Views *** */

CREATE VIEW naming_convention."vwCustomerAddresses" AS 
select a.customer_id, 
	c.first_name, 
	c.middle_name, 
	c.surname, 
	c.phone_no, 
	c.mobile_no, 
	c.email, 
	c.comments as customer_comments,
      a.id as customeraddress_id, 
	a.billing_or_delivery, 
	a.active, 
	a.line_1, 
	a.line_2, 
	a.line_3, 
	a.city, 
	a.state_id, 
	a.zip_postcode, 
	a.comments as customeraddress_comments
from naming_convention.customer as c, 
	naming_convention.customeraddress as a
where c.id = a.customer_id;

COMMENT ON VIEW naming_convention."vwCustomerAddresses"
	'This view returns all customer address information.';

CREATE VIEW naming_convention."vwCustomerOrders_AllDetails" AS 
select xcop.customerorder_id,
	o.order_date, 
	o.paid_date, 
	o.shipped_date, 
	o.total_price, 
	o.consignment_no,
	o.comments as customersorders_comments, 
	xcop.product_id, 
	xcop.quantity, 
	xcop.unit_price, 
	xcop.tax_amount, 
	xcop.discount_percentage, 
	xcop.comments as customersordersitems_comments,
	p.short_description,
	p.long_description,
	p.supplier_code,
	p.supplier_partno
from  naming_convention.customerorder o, 
	naming_convention.xref_customerorder_product xcop, 
	naming_convention.product p
where o.id = xcop.customerorder_id
	and   xcop.product_id = p.id;

CREATE VIEW naming_convention."vwCustomerOrderStatuses" AS 
select cos.customerorder_id,
	co.order_date, 
	co.paid_date, 
	co.shipped_date, 
	co.total_price, 
	co.consignment_no,
	co.comments as customersorders_comments, 
	cos.id as customerorderstatuses_id,
	cos.status_date, 
	cos.status_code, 
	cos.comments as customerorderstatus_comments

from naming_convention.customerorder co,
    naming_convention.customerorderstatus cos
where co.id = cos.customerorder_id;

/* Add List Data */
Insert into naming_convention.list_states(id, name) values(1, 'Alabama');
Insert into naming_convention.list_states(id, name) values(2, 'Alaska');
Insert into naming_convention.list_states(id, name) values(3, 'Arizona');
Insert into naming_convention.list_states(id, name) values(4, 'Arkansas');
Insert into naming_convention.list_states(id, name) values(5, 'California');
Insert into naming_convention.list_states(id, name) values(6, 'Colorado');
Insert into naming_convention.list_states(id, name) values(7, 'Connecticut');
Insert into naming_convention.list_states(id, name) values(8, 'Delaware');
Insert into naming_convention.list_states(id, name) values(9, 'Florida');
Insert into naming_convention.list_states(id, name) values(10, 'Georgia');
Insert into naming_convention.list_states(id, name) values(11, 'Hawaii');
Insert into naming_convention.list_states(id, name) values(12, 'Idaho');
Insert into naming_convention.list_states(id, name) values(13, 'Illinois');
Insert into naming_convention.list_states(id, name) values(14, 'Indiana');
Insert into naming_convention.list_states(id, name) values(15, 'Iowa');
Insert into naming_convention.list_states(id, name) values(16, 'Kansas');
Insert into naming_convention.list_states(id, name) values(17, 'Kentucky');
Insert into naming_convention.list_states(id, name) values(18, 'Louisiana');
Insert into naming_convention.list_states(id, name) values(19, 'Maine');
Insert into naming_convention.list_states(id, name) values(20, 'Maryland');
Insert into naming_convention.list_states(id, name) values(21, 'Massachusetts');
Insert into naming_convention.list_states(id, name) values(22, 'Michigan');
Insert into naming_convention.list_states(id, name) values(23, 'Minnesota');
Insert into naming_convention.list_states(id, name) values(24, 'Mississippi');
Insert into naming_convention.list_states(id, name) values(25, 'Missouri');
Insert into naming_convention.list_states(id, name) values(26, 'Montana');
Insert into naming_convention.list_states(id, name) values(27, 'Nebraska');
Insert into naming_convention.list_states(id, name) values(28, 'Nevada');
Insert into naming_convention.list_states(id, name) values(29, 'New Hampshire');
Insert into naming_convention.list_states(id, name) values(30, 'New Jersey');
Insert into naming_convention.list_states(id, name) values(31, 'New Mexico');
Insert into naming_convention.list_states(id, name) values(32, 'New York');
Insert into naming_convention.list_states(id, name) values(33, 'North Carolina');
Insert into naming_convention.list_states(id, name) values(34, 'North Dakota');
Insert into naming_convention.list_states(id, name) values(35, 'Ohio');
Insert into naming_convention.list_states(id, name) values(36, 'Oklahoma');
Insert into naming_convention.list_states(id, name) values(37, 'Oregon');
Insert into naming_convention.list_states(id, name) values(38, 'Pennsylvania');
Insert into naming_convention.list_states(id, name) values(39, 'Rhode Island');
Insert into naming_convention.list_states(id, name) values(40, 'South Carolina');
Insert into naming_convention.list_states(id, name) values(41, 'South Dakota');
Insert into naming_convention.list_states(id, name) values(42, 'Tennessee');
Insert into naming_convention.list_states(id, name) values(43, 'Texas');
Insert into naming_convention.list_states(id, name) values(44, 'Utah');
Insert into naming_convention.list_states(id, name) values(45, 'Vermont');
Insert into naming_convention.list_states(id, name) values(46, 'Virginia');
Insert into naming_convention.list_states(id, name) values(47, 'Washington');
Insert into naming_convention.list_states(id, name) values(48, 'West Virginia');
Insert into naming_convention.list_states(id, name) values(49, 'Wisconsin');
Insert into naming_convention.list_states(id, name) values(50, 'Wyoming');

/* Add History Tables and trigger functions */
CREATE TABLE naming_convention.hist_customer
(
    hist_id bigserial,
    id bigint,
    first_name character varying(50),
    middle_name character varying(50),
    surname character varying(50),
    date_of_birth date,
    phone_no character varying(20),
    mobile_no character varying(20),
    email character varying(100),
    comments text,
    action_date timestamp without time zone,
    action_user character varying(100),
    action_taken character varying (20),
    CONSTRAINT hist_customer_pkey PRIMARY KEY (hist_id)
)

TABLESPACE pg_default;

ALTER TABLE naming_convention.hist_customer
    OWNER to postgres;

GRANT ALL ON TABLE naming_convention.hist_customer TO postgres;
GRANT INSERT, UPDATE, SELECT, REFERENCES ON TABLE naming_convention.hist_customer TO PUBLIC;

CREATE or REPLACE FUNCTION naming_convention.tg_fn_customer()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_customer (id, 
		first_name,
		middle_name,
		surname,
		date_of_birth,
		phone_no,
		mobile_no,
		email,
		comments,
		action_date, action_user, action_taken) 
	VALUES (NEW.id, 
		NEW.first_name,
		NEW.middle_name,
		NEW.surname,
		NEW.date_of_birth,
		NEW.phone_no,
		NEW.mobile_no,
		NEW.email,
		NEW.comments,
		NOW(), user, arg_value);
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE or REPLACE FUNCTION naming_convention.tg_fn_del_customer()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_customer (id, 
		first_name,
		middle_name,
		surname,
		date_of_birth,
		phone_no,
		mobile_no,
		email,
		comments,
		action_date, action_user, action_taken) 
	VALUES (OLD.id, 
		OLD.first_name,
		OLD.middle_name,
		OLD.surname,
		OLD.date_of_birth,
		OLD.phone_no,
		OLD.mobile_no,
		OLD.email,
		OLD.comments,
		NOW(), user, arg_value);
	RETURN OLD;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER tg_insert_customer
   BEFORE INSERT
   ON naming_convention.customer
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_customer('INSERT');

CREATE TRIGGER tg_update_customer
   BEFORE UPDATE
   ON naming_convention.customer
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_customer('UPDATE');

CREATE TRIGGER tg_delete_customer
   BEFORE DELETE
   ON naming_convention.customer
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_del_customer('DELETE');


CREATE TABLE naming_convention.hist_customeraddress
(
    hist_id bigserial,
    id bigint,
    customer_id integer,
    billing_or_delivery bit(1),
    line_1 character varying(50),
    line_2 character varying(50),
    line_3 character varying(50),
    city character varying(100),
    state_id bigint,
    zip_postcode character varying(10),
    country character varying(100),
    comments text,
    action_date timestamp without time zone,
    action_user character varying(100),
    action_taken character varying (20),
    CONSTRAINT pk_histcustomeraddress PRIMARY KEY (hist_id)
)

TABLESPACE pg_default;

ALTER TABLE naming_convention.hist_customeraddress
    OWNER to postgres;

GRANT ALL ON TABLE naming_convention.hist_customeraddress TO postgres;

GRANT INSERT, UPDATE, SELECT, REFERENCES ON TABLE naming_convention.hist_customeraddress TO PUBLIC;

CREATE or REPLACE FUNCTION naming_convention.tg_fn_customeraddress()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_customeraddress (id, 
	    customer_id,
    	billing_or_delivery,
	    line_1,
    	line_2,
	    line_3,
    	city,
	    state_id,
    	zip_postcode,
	    country,
    	comments,
		action_date, action_user, action_taken) 
	VALUES (NEW.id, 
    	NEW.customer_id,
	    NEW.billing_or_delivery,
    	NEW.line_1,
	    NEW.line_2,
    	NEW.line_3,
	    NEW.city,
    	NEW.state_id,
	    NEW.zip_postcode,
    	NEW.country,
	    NEW.comments,
		NOW(), user, arg_value);
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE or REPLACE FUNCTION naming_convention.tg_fn_del_customeraddress()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_customeraddress (id, 
    	customer_id,
	    billing_or_delivery,
    	line_1,
	    line_2,
    	line_3,
	    city,
    	state_id,
	    zip_postcode,
    	country,
	    comments,
		action_date, action_user, action_taken) 
	VALUES (OLD.id, 
    	OLD.customer_id,
	    OLD.billing_or_delivery,
    	OLD.line_1,
	    OLD.line_2,
    	OLD.line_3,
	    OLD.city,
    	OLD.state_id,
	    OLD.zip_postcode,
    	OLD.country,
	    OLD.comments,
		NOW(), user, arg_value);
	RETURN OLD;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER tg_insert_customeraddress
   BEFORE INSERT
   ON naming_convention.customeraddress
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_customeraddress('INSERT');

CREATE TRIGGER tg_update_customeraddress
   BEFORE UPDATE
   ON naming_convention.customeraddress
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_customeraddress('UPDATE');

CREATE TRIGGER tg_delete_customeraddress
   BEFORE DELETE
   ON naming_convention.customeraddress
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_del_customeraddress('DELETE');

CREATE TABLE naming_convention.hist_customerorder
(
    hist_id bigserial,
    id bigint,
    customer_id integer,
    order_date date,
    paid_date date,
    shipped_date date,
    total_price double precision,
    special_instructions character varying(255),
    consignment_no character varying(50),
    comments text,
    action_date timestamp without time zone,
    action_user character varying(100),
    action_taken character varying (20),
    CONSTRAINT pk_hist_customerorder PRIMARY KEY (hist_id)
)

TABLESPACE pg_default;

ALTER TABLE naming_convention.hist_customerorder
    OWNER to postgres;

GRANT ALL ON TABLE naming_convention.hist_customerorder TO postgres;

GRANT INSERT, UPDATE, SELECT, REFERENCES ON TABLE naming_convention.hist_customerorder TO PUBLIC;

CREATE or REPLACE FUNCTION naming_convention.tg_fn_customerorder()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_customerorder (id, 
    	customer_id,
	    order_date,
    	paid_date,
	    shipped_date,
    	total_price,
	    special_instructions,
    	consignment_no,
	    comments,
		action_date, action_user, action_taken) 
	VALUES (NEW.id, 
    	NEW.customer_id,
	    NEW.order_date,
    	NEW.paid_date,
	    NEW.shipped_date,
    	NEW.total_price,
	    NEW.special_instructions,
    	NEW.consignment_no,
	    NEW.comments,
		NOW(), user, arg_value);
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE or REPLACE FUNCTION naming_convention.tg_fn_del_customerorder()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_customerorder (id, 
    	customer_id,
	    order_date,
    	paid_date,
	    shipped_date,
    	total_price,
	    special_instructions,
    	consignment_no,
	    comments,
		action_date, action_user, action_taken) 
	VALUES (OLD.id, 
    	OLD.customer_id,
	    OLD.order_date,
    	OLD.paid_date,
	    OLD.shipped_date,
    	OLD.total_price,
	    OLD.special_instructions,
    	OLD.consignment_no,
	    OLD.comments,
		NOW(), user, arg_value);
	RETURN OLD;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER tg_insert_customerorder
   BEFORE INSERT
   ON naming_convention.customerorder
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_customerorder('INSERT');

CREATE TRIGGER tg_update_customerorder
   BEFORE UPDATE
   ON naming_convention.customerorder
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_customerorder('UPDATE');

CREATE TRIGGER tg_delete_customerorder
   BEFORE DELETE
   ON naming_convention.customerorder
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_del_customerorder('DELETE');

CREATE TABLE naming_convention.hist_customerorderstatus
(
    hist_id bigserial,
    id bigint,
    customerorder_id bigint,
    status_date date,
    status_code character varying(10),
    comments text,
    action_date timestamp without time zone,
    action_user character varying(100),
    action_taken character varying (20),
    CONSTRAINT pk_hist_customerorderstatus PRIMARY KEY (hist_id)
)

TABLESPACE pg_default;

ALTER TABLE naming_convention.hist_customerorderstatus
    OWNER to postgres;

GRANT ALL ON TABLE naming_convention.hist_customerorderstatus TO postgres;

GRANT INSERT, UPDATE, SELECT, REFERENCES ON TABLE naming_convention.hist_customerorderstatus TO PUBLIC;

CREATE or REPLACE FUNCTION naming_convention.tg_fn_customerorderstatus()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_customerorderstatus (id, 
	    customerorder_id,
    	status_date,
	    status_code,
    	comments,
		action_date, action_user, action_taken) 
	VALUES (NEW.id, 
    	NEW.customerorder_id,
	    NEW.status_date,
    	NEW.status_code,
	    NEW.comments,
		NOW(), user, arg_value);
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE or REPLACE FUNCTION naming_convention.tg_fn_del_customerorderstatus()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_customerorderstatus (id, 
    	customerorder_id,
	    status_date,
    	status_code,
	    comments,
		action_date, action_user, action_taken) 
	VALUES (OLD.id, 
    	OLD.customerorder_id,
	    OLD.status_date,
    	OLD.status_code,
	    OLD.comments,
		NOW(), user, arg_value);
	RETURN OLD;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER tg_insert_customerorderstatus
   BEFORE INSERT
   ON naming_convention.customerorderstatus
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_customerorderstatus('INSERT');

CREATE TRIGGER tg_update_customerorderstatus
   BEFORE UPDATE
   ON naming_convention.customerorderstatus
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_customerorderstatus('UPDATE');

CREATE TRIGGER tg_delete_customerorderstatus
   BEFORE DELETE
   ON naming_convention.customerorderstatus
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_del_customerorderstatus('DELETE');

CREATE TABLE naming_convention.hist_product
(
    hist_id bigserial,
    id bigint,
    short_description character varying(255),
    unit_price double precision,
    long_description text,
    supplier_code character varying(20),
    supplier_partno character varying(50),
    comments text,
    action_date timestamp without time zone,
    action_user character varying(100),
    action_taken character varying (20),
    CONSTRAINT pk_hist_product PRIMARY KEY (hist_id)
)

TABLESPACE pg_default;

ALTER TABLE naming_convention.hist_product
    OWNER to postgres;

GRANT ALL ON TABLE naming_convention.hist_product TO postgres;

GRANT INSERT, UPDATE, SELECT, REFERENCES ON TABLE naming_convention.hist_product TO PUBLIC;

CREATE or REPLACE FUNCTION naming_convention.tg_fn_product()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_product (id, 
    	short_description,
	    unit_price,
    	long_description,
    	supplier_code,
	    supplier_partno,
    	comments,
		action_date, action_user, action_taken) 
	VALUES (NEW.id, 
    	NEW.short_description,
	    NEW.unit_price,
    	NEW.long_description,
	    NEW.supplier_code,
    	NEW.supplier_partno,
	    NEW.comments,
		NOW(), user, arg_value);
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE or REPLACE FUNCTION naming_convention.tg_fn_del_product()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_product (id, 
    	short_description,
	    unit_price,
    	long_description,
	    supplier_code,
    	supplier_partno,
	    comments,
		action_date, action_user, action_taken) 
	VALUES (OLD.id, 
    	OLD.short_description,
	    OLD.unit_price,
    	OLD.long_description,
	    OLD.supplier_code,
    	OLD.supplier_partno,
	    OLD.comments,
		NOW(), user, arg_value);
	RETURN OLD;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER tg_insert_product
   BEFORE INSERT
   ON naming_convention.product
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_product('INSERT');

CREATE TRIGGER tg_update_product
   BEFORE UPDATE
   ON naming_convention.product
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_product('UPDATE');

CREATE TRIGGER tg_delete_product
   BEFORE DELETE
   ON naming_convention.product
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_del_product('DELETE');

CREATE TABLE naming_convention.hist_supplier
(
    hist_id bigserial,
    supplier_code character varying(20),
    supplier_name character varying(50),
    contact_name character varying(100),
    contact_phone_no character varying(20),
    contact_email character varying(100),
    address_line1 character varying(50),
    address_line2 character varying(50),
    address_line3 character varying(50),
    city character varying(100),
    state_id bigint,
    zip_postcode character varying(10),
    country character varying(50),
    comments text,
    action_date timestamp without time zone,
    action_user character varying(100),
    action_taken character varying (20),
    CONSTRAINT pk_hist_supplier PRIMARY KEY (hist_id)
)

TABLESPACE pg_default;

ALTER TABLE naming_convention.hist_supplier
    OWNER to postgres;

GRANT ALL ON TABLE naming_convention.hist_supplier TO postgres;

GRANT INSERT, UPDATE, SELECT, REFERENCES ON TABLE naming_convention.hist_supplier TO PUBLIC;


CREATE or REPLACE FUNCTION naming_convention.tg_fn_supplier()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_supplier (supplier_code,
    	supplier_name,
	    contact_name,
    	contact_phone_no,
	    contact_email,
    	address_line1,
	    address_line2,
    	address_line3,
	    city,
    	state_id,
	    zip_postcode,
    	country,
	    comments,
		action_date, action_user, action_taken) 
	VALUES (NEW.supplier_code,
    	NEW.supplier_name,
	    NEW.contact_name,
    	NEW.contact_phone_no,
	    NEW.contact_email,
    	NEW.address_line1,
	    NEW.address_line2,
    	NEW.address_line3,
	    NEW.city,
    	NEW.state_id,
	    NEW.zip_postcode,
    	NEW.country,
	    NEW.comments,
		NOW(), user, arg_value);
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';


CREATE or REPLACE FUNCTION naming_convention.tg_fn_del_supplier()
	RETURNS trigger AS $$
	DECLARE 
    		arg_value varchar;
	BEGIN
    		arg_value := TG_ARGV[0];

	INSERT INTO naming_convention.hist_supplier (supplier_code,
    	supplier_name,
	    contact_name,
    	contact_phone_no,
	    contact_email,
    	address_line1,
	    address_line2,
    	address_line3,
	    city,
    	state_id,
	    zip_postcode,
    	country,
	    comments,
		action_date, action_user, action_taken) 
	VALUES (OLD.supplier_code,
    	OLD.supplier_name,
	    OLD.contact_name,
    	OLD.contact_phone_no,
	    OLD.contact_email,
    	OLD.address_line1,
	    OLD.address_line2,
    	OLD.address_line3,
	    OLD.city,
    	OLD.state_id,
	    OLD.zip_postcode,
    	OLD.country,
	    OLD.comments,
		NOW(), user, arg_value);
	RETURN OLD;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER tg_insert_supplier
   BEFORE INSERT
   ON naming_convention.supplier
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_supplier('INSERT');

CREATE TRIGGER tg_update_supplier
   BEFORE UPDATE
   ON naming_convention.supplier
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_supplier('UPDATE');

CREATE TRIGGER tg_delete_supplier
   BEFORE DELETE
   ON naming_convention.supplier
   FOR EACH ROW
   EXECUTE PROCEDURE naming_convention.tg_fn_del_supplier('DELETE');

/* Insert, Update, Delete Test Actions */
/* Customer */
INSERT INTO naming_convention.customer(first_name,middle_name,
    surname,date_of_birth,phone_no,
    mobile_no, email, comments)
VALUES ('First Name', 'M int', 'Last Name', '2021/04/13', '(123)456-7890', '987-654-3210', 'test@test.com', 'Comments'); 

INSERT INTO naming_convention.customer(first_name,middle_name,
    surname,date_of_birth,phone_no,
    mobile_no, email, comments)
VALUES ('First Name 2', 'M int2', 'Last Name2', '2021/04/13', '(123)456-7890', '987-654-3210', 'test@test2.com', 'Comments 2'); 

INSERT INTO naming_convention.customer(first_name,middle_name,
    surname,date_of_birth,phone_no,
    mobile_no, email, comments)
VALUES ('First Name 2', 'M int2', 'Last Name2', '2021/04/13', '(123)456-7890', '987-654-3210', 'testx@test2.com', 'Comments 2'); 

UPDATE naming_convention.customer SET first_name = 'My First Name' where id=4;

DELETE FROM naming_convention.customer where id=2;


/* supplier */
Insert into naming_convention.supplier (supplier_code, supplier_name, contact_name, contact_phone_no, contact_email, address_line1, city, state_id, zip_postcode) VALUES ('ABC123', 'Supplier 1', 'Contact 1', '(123)-456-7890', 'test@test.com', '123 Street', 'Dallas', 45, '12345');

Insert into naming_convention.supplier (supplier_code, supplier_name, contact_name, contact_phone_no, contact_email, address_line1, city, state_id, zip_postcode) VALUES ('DEF123', 'Supplier 2', 'Contact 2', '(123)-456-7890', 'test2@test.com', '456 Street', 'Dallas', 45, '12345');

Insert into naming_convention.supplier (supplier_code, supplier_name, contact_name, contact_phone_no, contact_email, address_line1, city, state_id, zip_postcode) VALUES ('GHU123', 'Supplier 3', 'Contact 3', '(123)-456-7890', 'test3@test.com', '789 Street', 'Dallas', 45, '12345');

UPDATE naming_convention.supplier set supplier_name='Updated Name' where supplier_code = 'ABC123';

DELETE from naming_convention.supplier where supplier_code = 'GHU123';

/* customeraddress */
INSERT into naming_convention.customeraddress (customer_id, billing_or_delivery, active, line_1, city, state_id, comments) VALUES (1, '1', '1', 'Address Line 1', 'Minot', 43, 'Comments go here');

INSERT into naming_convention.customeraddress (customer_id, billing_or_delivery, active, line_1, city, state_id, comments) VALUES (4, '0', '0', 'Address Line 1', 'DALLAS', 43, 'Comments go here');

INSERT into naming_convention.customeraddress (customer_id, billing_or_delivery, active, line_1, city, state_id, comments) VALUES (4, '0', '0', 'Address Line X', 'DALLAS', 43, 'Comments go here');

UPDATE naming_convention.customeraddress set comments = 'Updated Comments' where id=3;
 
DELETE FROM naming_convention.customeraddress where ID = 2;

/* customerorder */
INSERT INTO naming_convention.customerorder (customer_id, order_date, total_price, comments) VALUES (1, '2021/04/12', 125.00, 'Comments go here');

INSERT INTO naming_convention.customerorder (customer_id, order_date, total_price, comments) VALUES (4, '2021/04/13', 225.00, 'Comments go here');

INSERT INTO naming_convention.customerorder (customer_id, order_date, total_price, comments) VALUES (4, '2021/04/14', 325.00, 'Comments go here');

UPDATE naming_convention.customerorder set shipped_date = '2021/04/14' where id=2;

DELETE FROM naming_convention.customerorder where order_date = '2021/04/14';

/* product */
Insert into naming_convention.product (short_description, unit_price, supplier_code, supplier_partno, comments) VALUES ('ShortDescription1', 25.00, 'ABC123', '1001', 'Comment 1');

Insert into naming_convention.product (short_description, unit_price, supplier_code, supplier_partno, comments) VALUES ('ShortDescription2', 30.00, 'ABC123', '1002', 'Comment 2');

Insert into naming_convention.product (short_description, unit_price, supplier_code, supplier_partno, comments) VALUES ('ShortDescription3', 35.00, 'ABC123', '1003', 'Comment 3');

UPDATE naming_convention.product set comments = 'Updated Comment' where supplier_partno = '1002';

DELETE from naming_convention.product where supplier_partno = '1003';

/* customerorderstatus */
INSERT into naming_convention.customerorderstatus (customerorder_id, status_date, status_code, comments) VALUES (1, '2021/04/14', 'Stocked', 'Comments Here');  

INSERT into naming_convention.customerorderstatus (customerorder_id, status_date, status_code, comments) VALUES (2, '2021/04/14', 'Shipped', 'Comments Here2');  

INSERT into naming_convention.customerorderstatus (customerorder_id, status_date, status_code, comments) VALUES (2, '2021/04/14', 'Shipped', 'Comments Here3');  

UPDATE naming_convention.customerorderstatus set status_code = 'OnHold' where comments='Comments Here2'; 

delete from naming_convention.customerorderstatus where status_code='Stocked';
